package main

import (
	"context"
	"osg_intern/api"
	handler "osg_intern/api/handler"
	"osg_intern/config"
	"osg_intern/pkg/logger"
	"osg_intern/service"
	"osg_intern/storage/db"

	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/golang-migrate/migrate/v4/source/github"
)

func main() {

	cfg := config.Load()
	log := logger.NewLogger("mini-project", logger.LevelInfo)

	strg, err := db.NewPostgres(context.Background(), *cfg)

	if err != nil {
		return
	}

	// if cfg.PostgresHost == "localhost" {
	// 	m, err := migrate.New(
	// 		fmt.Sprintf("file://%s", cfg.MigrationPath),
	// 		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
	// 			cfg.PostgresUser,
	// 			cfg.PostgresPassword,
	// 			cfg.PostgresHost,
	// 			cfg.PostgresPort,
	// 			cfg.PostgresDatabase,
	// 		),
	// 	)

	// 	if err != nil {
	// 		log.Panic("migrate.Postgres", logger.Error(err))
	// 	}

	// 	if err := m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
	// 		log.Panic("migrate.Postgres", logger.Error(err))
	// 	}
	// }

	service := service.New(*cfg, strg, log)

	h := handler.NewHandler(strg, *cfg, log, service)

	r := api.NewServer(h)
	r.Run(":8080")

}
