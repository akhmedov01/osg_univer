package storage

import (
	"context"
	"osg_intern/api/module"
)

type StorageI interface {
	User() UserI
	Role() RoleI
	Session() SessionI
	Department() DepartmentI
	District() DistrictI
	Position() PositionI
	Region() RegionI
	Republic() RepublicI
}

type UserI interface {
	Create(ctx context.Context, req *module.CretaeUser) (resp *module.User, err error)
	Update(ctx context.Context, req *module.User) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.GetByPKey) (res *module.User, err error)
	GetList(ctx context.Context, req *module.GetListUserReq) (resp *module.GetListUserResp, err error)
	Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error)
	GetUserByUsername(ctx context.Context, req *module.GetUserByUserName) (res *module.User, err error)
}

type RoleI interface {
	Create(ctx context.Context, req *module.Role) (res *module.Role, err error)
	Update(ctx context.Context, req *module.Role) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.PrimaryKeyUUID) (res *module.Role, err error)
	GetList(ctx context.Context, req *module.RoleGetListReq) (resp *module.GetRoleListRes, err error)
	Delete(ctx context.Context, req *module.PrimaryKeyUUID) (rowsAffected int64, err error)
}

type SessionI interface {
	Create(ctx context.Context, in *module.CreateSessionReq) (pKey *module.Session, err error)
	GetList(ctx context.Context, in *module.SessionGetListReq) (res *module.GetSessionListRes, err error)
	GetByPK(ctx context.Context, in *module.PrimaryKeyUUID) (res *module.Session, err error)
	Update(ctx context.Context, in *module.UpdateSessionReq) (res *module.Session, err error)
	Delete(ctx context.Context, in *module.PrimaryKeyUUID) (rowsAffected int64, err error)
	DeleteExpiredUserSessions(ctx context.Context, userID int) (rowsAffected int64, err error)
	GetSessionListByUserID(ctx context.Context, userID int) (res *module.GetSessionListRes, err error)
}

type DepartmentI interface {
	Create(ctx context.Context, req *module.CreateDepartment) (res *module.Department, err error)
	Update(ctx context.Context, req *module.Department) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.GetByPKey) (res *module.Department, err error)
	GetList(ctx context.Context, req *module.GetListDepartmentReq) (resp *module.GetListDepartmentRes, err error)
	Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error)
}

type DistrictI interface {
	Create(ctx context.Context, req *module.CreateDistrict) (res *module.District, err error)
	Update(ctx context.Context, req *module.District) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.GetByPKey) (res *module.District, err error)
	GetList(ctx context.Context, req *module.GetListDistrictReq) (resp *module.GetListDistrictRes, err error)
	Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error)
}

type PositionI interface {
	Create(ctx context.Context, req *module.CreatePosition) (res *module.Position, err error)
	Update(ctx context.Context, req *module.Position) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.GetByPKey) (res *module.Position, err error)
	GetList(ctx context.Context, req *module.GetListPositionReq) (resp *module.GetListPositionRes, err error)
	Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error)
}

type RegionI interface {
	Create(ctx context.Context, req *module.CreateRegion) (res *module.Region, err error)
	Update(ctx context.Context, req *module.Region) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.GetByPKey) (res *module.Region, err error)
	GetList(ctx context.Context, req *module.GetListRegionReq) (resp *module.GetListRegionRes, err error)
	Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error)
}

type RepublicI interface {
	Create(ctx context.Context, req *module.CreateRepublic) (res *module.Republic, err error)
	Update(ctx context.Context, req *module.Republic) (rowsAffected int64, err error)
	Get(ctx context.Context, req *module.GetByPKey) (res *module.Republic, err error)
	GetList(ctx context.Context, req *module.GetListRepublicReq) (resp *module.GetListRepublicRes, err error)
	Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error)
}
