package db

import (
	"context"
	"fmt"

	"osg_intern/config"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db         *pgxpool.Pool
	user       storage.UserI
	role       storage.RoleI
	session    storage.SessionI
	department storage.DepartmentI
	district   storage.DistrictI
	position   storage.PositionI
	region     storage.RegionI
	republic   storage.RepublicI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	fmt.Println("psql creds::")
	fmt.Printf("postgres://%s:%s@%s:%d/%s?sslmode=disable\n",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) User() storage.UserI {
	if s.user == nil {
		s.user = NewUserRepo(s.db)
	}
	return s.user
}

func (s *Store) Role() storage.RoleI {
	if s.role == nil {
		s.role = NewRoleRepo(s.db)
	}
	return s.role
}

func (s *Store) Session() storage.SessionI {
	if s.session == nil {
		s.session = NewSessionRepo(s.db)
	}
	return s.session
}

func (s *Store) Department() storage.DepartmentI {
	if s.department == nil {
		s.department = NewDepartmentRepo(s.db)
	}
	return s.department
}

func (s *Store) District() storage.DistrictI {
	if s.district == nil {
		s.district = NewDistrictRepo(s.db)
	}
	return s.district
}

func (s *Store) Position() storage.PositionI {
	if s.position == nil {
		s.position = NewPositionRepo(s.db)
	}
	return s.position
}

func (s *Store) Region() storage.RegionI {
	if s.region == nil {
		s.region = NewRegionRepo(s.db)
	}
	return s.region
}

func (s *Store) Republic() storage.RepublicI {
	if s.republic == nil {
		s.republic = NewRepublicRepo(s.db)
	}
	return s.republic
}
