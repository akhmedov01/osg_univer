package db

import (
	"context"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type RoleRepo struct {
	db *pgxpool.Pool
}

func NewRoleRepo(db *pgxpool.Pool) storage.RoleI {
	return &RoleRepo{
		db: db,
	}
}

// CREATE ROLE WITH ROLE_USER RELATION TABLE
// GET LIST BY USER ID

func (s *RoleRepo) Create(ctx context.Context, req *module.Role) (res *module.Role, err error) {
	fmt.Println("req:::::", req)
	query := `INSERT INTO "role" (
		id,                 
        role_type,
        user_id
	) VALUES (
		$1,
		$2,
		$3
	)`

	id, err := uuid.NewRandom()
	if err != nil {
		return nil, err
	}

	_, err = s.db.Exec(ctx, query,
		id.String(),
		req.RoleType,
		req.UserId,
	)

	res = &module.Role{
		Id:       id.String(),
		RoleType: req.RoleType,
		UserId:   req.UserId,
	}

	return res, err
}

func (s *RoleRepo) Get(ctx context.Context, req *module.PrimaryKeyUUID) (res *module.Role, err error) {
	res = &module.Role{}

	query := `SELECT
		id,                 
    	role_type,
    	user_id,
	FROM
		"role"
	WHERE
		id = $1`

	err = s.db.QueryRow(
		ctx,
		query,
		req.Id).Scan(
		&res.Id,
		&res.RoleType,
		&res.UserId,
	)

	if err != nil {
		return res, err
	}

	return res, nil
}

func (s *RoleRepo) GetList(ctx context.Context, req *module.RoleGetListReq) (res *module.GetRoleListRes, err error) {
	res = &module.GetRoleListRes{}
	params := make(map[string]interface{})
	var arr []interface{}
	fmt.Println("req", req)

	query := `SELECT
		id,                 
    	role_type,
    	user_id
	FROM
		"role"`
	filter := " WHERE 1=1"

	offset := " OFFSET 0"

	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND (role_type ILIKE '%' || :search || '%')`
	}

	if req.UserId > 0 {
		params["user_id"] = req.UserId
		filter += " AND user_id = :user_id"
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "role"` + filter

	cQ, arr = helper.ReplaceQueryParams(cQ, params)

	fmt.Println("cQ", cQ)

	err = s.db.QueryRow(ctx, cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := s.db.Query(ctx, q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &module.Role{}

		err = rows.Scan(
			&obj.Id,
			&obj.RoleType,
			&obj.UserId,
		)
		if err != nil {
			return res, err
		}

		res.Roles = append(res.Roles, *obj)
	}

	return res, nil
}
func (s *RoleRepo) Update(ctx context.Context, req *module.Role) (rowsAffected int64, err error) {
	query := `UPDATE "role" SET                
    	role_type = :role_type
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":        req.Id,
		"role_type": req.RoleType,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := s.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
func (s *RoleRepo) Delete(ctx context.Context, req *module.PrimaryKeyUUID) (rowsAffected int64, err error) {
	query := `DELETE FROM "role" WHERE id = $1`

	result, err := s.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
