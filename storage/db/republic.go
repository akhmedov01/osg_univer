package db

import (
	"context"
	"encoding/json"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type RepublicRepo struct {
	db *pgxpool.Pool
}

func NewRepublicRepo(db *pgxpool.Pool) storage.RepublicI {
	return &RepublicRepo{
		db: db,
	}
}

func (s *RepublicRepo) Create(ctx context.Context, req *module.CreateRepublic) (res *module.Republic, err error) {
	fmt.Println("req:::::", req)

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return res, err
	}

	var name []byte
	var dep module.Republic

	query := `
	INSERT INTO 
		"republic" (name) 
	VALUES 
		($1)
	RETURNING
		id,
		name`

	err = s.db.QueryRow(ctx, query,
		nameInByte,
	).Scan(
		&dep.Id,
		&name,
	)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(name, &dep.Name)

	if err != nil {
		return nil, err
	}

	return &dep, err
}

func (s *RepublicRepo) Get(ctx context.Context, req *module.GetByPKey) (res *module.Republic, err error) {

	res = &module.Republic{}

	query := `SELECT
		id,                 
    	name
	FROM
		"republic"
	WHERE
		id = $1`

	err = s.db.QueryRow(
		ctx,
		query,
		req.Id).Scan(
		&res.Id,
		&res.Name,
	)

	if err != nil {
		return res, err
	}

	return res, nil
}

func (s *RepublicRepo) GetList(ctx context.Context, req *module.GetListRepublicReq) (res *module.GetListRepublicRes, err error) {
	res = &module.GetListRepublicRes{}
	params := make(map[string]interface{})
	var arr []interface{}
	fmt.Println("req", req)

	query := `SELECT
		id,                 
    	name
	FROM
		"republic"`

	filter := " WHERE 1=1"

	offset := " OFFSET 0"

	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND (name ILIKE '%' || :search || '%')`
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "republic"` + filter

	cQ, arr = helper.ReplaceQueryParams(cQ, params)

	fmt.Println("cQ", cQ)

	err = s.db.QueryRow(ctx, cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := s.db.Query(ctx, q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &module.Republic{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
		)
		if err != nil {
			return res, err
		}

		res.Republics = append(res.Republics, *obj)
	}

	return res, nil
}
func (s *RepublicRepo) Update(ctx context.Context, req *module.Republic) (rowsAffected int64, err error) {

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return 0, err
	}

	query := `UPDATE "republic" SET                
    	name = :name
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":   req.Id,
		"name": nameInByte,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := s.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
func (s *RepublicRepo) Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error) {
	query := `DELETE FROM "republic" WHERE id = $1`

	result, err := s.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
