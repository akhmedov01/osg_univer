package db

import (
	"context"
	"encoding/json"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type DepartmentRepo struct {
	db *pgxpool.Pool
}

func NewDepartmentRepo(db *pgxpool.Pool) storage.DepartmentI {
	return &DepartmentRepo{
		db: db,
	}
}

func (s *DepartmentRepo) Create(ctx context.Context, req *module.CreateDepartment) (res *module.Department, err error) {
	fmt.Println("req:::::", req)

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return res, err
	}

	var name []byte
	var dep module.Department

	query := `
	INSERT INTO 
		"department" (name) 
	VALUES 
		($1)
	RETURNING
		id,
		name`

	err = s.db.QueryRow(ctx, query,
		nameInByte,
	).Scan(
		&dep.Id,
		&name,
	)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(name, &dep.Name)

	if err != nil {
		return nil, err
	}

	return &dep, err
}

func (s *DepartmentRepo) Get(ctx context.Context, req *module.GetByPKey) (res *module.Department, err error) {

	res = &module.Department{}

	query := `SELECT
		id,                 
    	name
	FROM
		"department"
	WHERE
		id = $1`

	err = s.db.QueryRow(
		ctx,
		query,
		req.Id).Scan(
		&res.Id,
		&res.Name,
	)

	if err != nil {
		return res, err
	}

	return res, nil
}

func (s *DepartmentRepo) GetList(ctx context.Context, req *module.GetListDepartmentReq) (res *module.GetListDepartmentRes, err error) {
	res = &module.GetListDepartmentRes{}
	params := make(map[string]interface{})
	var arr []interface{}
	fmt.Println("req", req)

	query := `SELECT
		id,                 
    	name
	FROM
		"department"`

	filter := " WHERE 1=1"

	offset := " OFFSET 0"

	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND (name ILIKE '%' || :search || '%')`
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "department"` + filter

	cQ, arr = helper.ReplaceQueryParams(cQ, params)

	fmt.Println("cQ", cQ)

	err = s.db.QueryRow(ctx, cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := s.db.Query(ctx, q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &module.Department{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
		)
		if err != nil {
			return res, err
		}

		res.Departments = append(res.Departments, *obj)
	}

	return res, nil
}
func (s *DepartmentRepo) Update(ctx context.Context, req *module.Department) (rowsAffected int64, err error) {

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return 0, err
	}

	query := `UPDATE "department" SET                
    	name = :name
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":   req.Id,
		"name": nameInByte,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := s.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
func (s *DepartmentRepo) Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error) {
	query := `DELETE FROM "department" WHERE id = $1`

	result, err := s.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
