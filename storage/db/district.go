package db

import (
	"context"
	"encoding/json"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type DistrictRepo struct {
	db *pgxpool.Pool
}

func NewDistrictRepo(db *pgxpool.Pool) storage.DistrictI {
	return &DistrictRepo{
		db: db,
	}
}

func (s *DistrictRepo) Create(ctx context.Context, req *module.CreateDistrict) (res *module.District, err error) {
	fmt.Println("req:::::", req)

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return res, err
	}

	var name []byte
	var dep module.District

	query := `
	INSERT INTO 
		"district" (name, region_id) 
	VALUES 
		($1,$2)
	RETURNING
		id,
		name,
		region_id`

	err = s.db.QueryRow(ctx, query,
		nameInByte,
	).Scan(
		&dep.Id,
		&name,
		&req.RegionId,
	)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(name, &dep.Name)

	if err != nil {
		return nil, err
	}

	return &dep, err
}

func (s *DistrictRepo) Get(ctx context.Context, req *module.GetByPKey) (res *module.District, err error) {

	res = &module.District{}

	query := `SELECT
		id,                 
    	name,
		region_id
	FROM
		"district"
	WHERE
		id = $1`

	err = s.db.QueryRow(
		ctx,
		query,
		req.Id).Scan(
		&res.Id,
		&res.Name,
		&res.RegionId,
	)

	if err != nil {
		return res, err
	}

	return res, nil
}

func (s *DistrictRepo) GetList(ctx context.Context, req *module.GetListDistrictReq) (res *module.GetListDistrictRes, err error) {
	res = &module.GetListDistrictRes{}
	params := make(map[string]interface{})
	var arr []interface{}
	fmt.Println("req", req)

	query := `SELECT
		id,                 
    	name,
		region_id
	FROM
		"District"`

	filter := " WHERE 1=1"

	offset := " OFFSET 0"

	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND (name ILIKE '%' || :search || '%')`
	}

	if req.RegionId > 0 {
		filter += `, region_id = :region_id`
		params["region_id"] = req.RegionId
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "district"` + filter

	cQ, arr = helper.ReplaceQueryParams(cQ, params)

	fmt.Println("cQ", cQ)

	err = s.db.QueryRow(ctx, cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := s.db.Query(ctx, q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &module.District{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
			&obj.RegionId,
		)
		if err != nil {
			return res, err
		}

		res.Districts = append(res.Districts, *obj)
	}

	return res, nil
}
func (s *DistrictRepo) Update(ctx context.Context, req *module.District) (rowsAffected int64, err error) {

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return 0, err
	}

	query := `UPDATE "district" SET                
    	name = :name, region_id = :region_id
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":        req.Id,
		"name":      nameInByte,
		"region_id": req.RegionId,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := s.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
func (s *DistrictRepo) Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error) {
	query := `DELETE FROM "district" WHERE id = $1`

	result, err := s.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
