package db

import (
	"context"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/config"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type userRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserI {
	return &userRepo{
		db: db,
	}
}

func (u *userRepo) Create(ctx context.Context, req *module.CretaeUser) (resp *module.User, err error) {

	query := `
	INSERT INTO 
		"user" (username, password, full_name, birth_date, avatar, birth_district_id) 
	VALUES
		($1,$2,$3,$4,$5,$6)
	RETURNING 
		id`

	var user module.User

	err = u.db.QueryRow(ctx, query,
		req.Username,
		req.Password,
		req.FullName,
		req.BirthDate,
		req.Avatar,
		req.BirthDistrictId,
	).Scan(
		&user.Id,
		&user.Username,
		&user.FullName,
		&user.BirthDate,
		&user.Avatar,
		&user.BirthDistrictId,
		&user.CreatedAt,
		&user.UpdatedAt,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &user, nil
}
func (u *userRepo) Update(ctx context.Context, req *module.User) (rowsAffected int64, err error) {

	validRole := map[string]bool{
		"EMPLOYEE": true,
		"STUDENT":  true,
	}

	params := make(map[string]interface{})

	querySet := `UPDATE "user" SET
				updated_at = CURRENT_TIMESTAMP`

	filter := ` WHERE id = :id`
	params["id"] = req.Id

	if _, ok := validRole[req.Role]; ok {
		querySet += `, role = :role`
		params["role"] = req.Role
	}

	if req.Username != "" {
		querySet += `, username = :username`
		params["username"] = req.Username
	}

	if req.Password != "" {
		querySet += `, password = :password`
		params["password"] = req.Password
	}

	if req.FullName != "" {
		querySet += `, full_name = :full_name`
		params["full_name"] = req.FullName
	}

	if req.BirthDate != "" {
		querySet += `, birth_date = :birth_date`
		params["birth_date"] = req.BirthDate
	}

	if req.BirthDistrictId != 0 {
		querySet += `, birth_district_id = :birth_district_id`
		params["birth_district_id"] = req.BirthDistrictId
	}

	query := querySet + filter
	q, arr := helper.ReplaceQueryParams(query, params)

	_, err = u.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	return 0, nil

}
func (u *userRepo) Get(ctx context.Context, req *module.GetByPKey) (res *module.User, err error) {

	query := `
	SELECT 
		id,
		username,
		full_name,
		birth_date,
		avatar,
		birth_district_id,
		TO_CHAR(created_at, ` + config.DatabaseQueryTimeLayout + `) AS created_at,
		TO_CHAR(updated_at, ` + config.DatabaseQueryTimeLayout + `) AS updated_at
	FROM 
		"user"
	WHERE 
		id=$1`

	resp := u.db.QueryRow(ctx, query, req.Id)

	var user module.User

	err = resp.Scan(
		&user.Id,
		&user.Username,
		&user.FullName,
		&user.BirthDate,
		&user.Avatar,
		&user.BirthDistrictId,
		&user.CreatedAt,
		&user.UpdatedAt,
	)

	if err != nil {
		fmt.Println("Error from Select User")
		return &module.User{}, err
	}

	return &user, nil
}
func (u *userRepo) GetList(ctx context.Context, req *module.GetListUserReq) (resp *module.GetListUserResp, err error) {

	params := make(map[string]interface{})

	query := `
	SELECT
		id,
		username,
		full_name,
		birth_date,
		avatar,
		birth_district_id
		TO_CHAR(created_at, ` + config.DatabaseQueryTimeLayout + `) AS created_at,
		TO_CHAR(updated_at, ` + config.DatabaseQueryTimeLayout + `) AS updated_at
	FROM
		"user`

	filter := " WHERE 1=1"
	offset := " OFFSET 0"
	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND ((user_name) ILIKE ('%' || :search || '%'))
					OR (full_name ILIKE '%' || :search || '%')`
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "user"` + filter
	cQ, arr := helper.ReplaceQueryParams(cQ, params)
	err = u.db.QueryRow(ctx, cQ, arr...).Scan(
		&resp.Count,
	)
	if err != nil {
		return resp, err
	}

	q := query + filter + offset + limit
	q, arr = helper.ReplaceQueryParams(q, params)

	rows, err := u.db.Query(ctx, q, arr...)
	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		user := module.User{}
		err = rows.Scan(
			&user.Id,
			&user.Username,
			&user.FullName,
			&user.BirthDate,
			&user.Avatar,
			&user.BirthDistrictId,
			&user.CreatedAt,
			&user.UpdatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Users = append(resp.Users, user)
	}

	return resp, nil

}
func (u *userRepo) Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error) {
	query := `DELETE FROM "user" WHERE id = $1`

	_, err = u.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	return 0, err
}

func (u *userRepo) GetUserByUsername(ctx context.Context, req *module.GetUserByUserName) (res *module.User, err error) {

	return &module.User{}, nil
}
