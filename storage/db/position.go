package db

import (
	"context"
	"encoding/json"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type PositionRepo struct {
	db *pgxpool.Pool
}

func NewPositionRepo(db *pgxpool.Pool) storage.PositionI {
	return &PositionRepo{
		db: db,
	}
}

func (s *PositionRepo) Create(ctx context.Context, req *module.CreatePosition) (res *module.Position, err error) {
	fmt.Println("req:::::", req)

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return res, err
	}

	var name []byte
	var dep module.Position

	query := `
	INSERT INTO 
		"position" (name) 
	VALUES 
		($1)
	RETURNING
		id,
		name`

	err = s.db.QueryRow(ctx, query,
		nameInByte,
	).Scan(
		&dep.Id,
		&name,
	)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(name, &dep.Name)

	if err != nil {
		return nil, err
	}

	return &dep, err
}

func (s *PositionRepo) Get(ctx context.Context, req *module.GetByPKey) (res *module.Position, err error) {

	res = &module.Position{}

	query := `SELECT
		id,                 
    	name
	FROM
		"position"
	WHERE
		id = $1`

	err = s.db.QueryRow(
		ctx,
		query,
		req.Id).Scan(
		&res.Id,
		&res.Name,
	)

	if err != nil {
		return res, err
	}

	return res, nil
}

func (s *PositionRepo) GetList(ctx context.Context, req *module.GetListPositionReq) (res *module.GetListPositionRes, err error) {
	res = &module.GetListPositionRes{}
	params := make(map[string]interface{})
	var arr []interface{}
	fmt.Println("req", req)

	query := `SELECT
		id,                 
    	name
	FROM
		"position"`

	filter := " WHERE 1=1"

	offset := " OFFSET 0"

	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND (name ILIKE '%' || :search || '%')`
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "position"` + filter

	cQ, arr = helper.ReplaceQueryParams(cQ, params)

	fmt.Println("cQ", cQ)

	err = s.db.QueryRow(ctx, cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := s.db.Query(ctx, q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &module.Position{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
		)
		if err != nil {
			return res, err
		}

		res.Positions = append(res.Positions, *obj)
	}

	return res, nil
}
func (s *PositionRepo) Update(ctx context.Context, req *module.Position) (rowsAffected int64, err error) {

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return 0, err
	}

	query := `UPDATE "position" SET                
    	name = :name
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":   req.Id,
		"name": nameInByte,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := s.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
func (s *PositionRepo) Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error) {
	query := `DELETE FROM "Position" WHERE id = $1`

	result, err := s.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
