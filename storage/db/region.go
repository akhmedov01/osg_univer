package db

import (
	"context"
	"encoding/json"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/pkg/helper"
	"osg_intern/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

type RegionRepo struct {
	db *pgxpool.Pool
}

func NewRegionRepo(db *pgxpool.Pool) storage.RegionI {
	return &RegionRepo{
		db: db,
	}
}

func (s *RegionRepo) Create(ctx context.Context, req *module.CreateRegion) (res *module.Region, err error) {
	fmt.Println("req:::::", req)

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return res, err
	}

	var name []byte
	var dep module.Region

	query := `
	INSERT INTO 
		"region" (name, republic_id) 
	VALUES 
		($1,$2)
	RETURNING
		id,
		name,
		republic_id`

	err = s.db.QueryRow(ctx, query,
		nameInByte,
	).Scan(
		&dep.Id,
		&name,
		&req.RepublicId,
	)

	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(name, &dep.Name)

	if err != nil {
		return nil, err
	}

	return &dep, err
}

func (s *RegionRepo) Get(ctx context.Context, req *module.GetByPKey) (res *module.Region, err error) {

	res = &module.Region{}

	query := `SELECT
		id,                 
    	name,
		republic_id
	FROM
		"region"
	WHERE
		id = $1`

	err = s.db.QueryRow(
		ctx,
		query,
		req.Id).Scan(
		&res.Id,
		&res.Name,
		&res.RepublicId,
	)

	if err != nil {
		return res, err
	}

	return res, nil
}

func (s *RegionRepo) GetList(ctx context.Context, req *module.GetListRegionReq) (res *module.GetListRegionRes, err error) {
	res = &module.GetListRegionRes{}
	params := make(map[string]interface{})
	var arr []interface{}
	fmt.Println("req", req)

	query := `SELECT
		id,                 
    	name,
		republic_id
	FROM
		"region"`

	filter := " WHERE 1=1"

	offset := " OFFSET 0"

	limit := " LIMIT 10"

	if len(req.Search) > 0 {
		params["search"] = req.Search
		filter += ` AND (name ILIKE '%' || :search || '%')`
	}

	if req.RepublicId > 0 {
		filter += `, republic_id = :republic_id`
		params["republic_id"] = req.RepublicId
	}

	if req.Page > 0 {
		params["offset"] = req.Page
		offset = " OFFSET :offset"
	}

	if req.Limit > 0 {
		params["limit"] = req.Limit
		limit = " LIMIT :limit"
	}

	cQ := `SELECT count(1) FROM "region"` + filter

	cQ, arr = helper.ReplaceQueryParams(cQ, params)

	fmt.Println("cQ", cQ)

	err = s.db.QueryRow(ctx, cQ, arr...).Scan(
		&res.Count,
	)
	if err != nil {
		return res, err
	}

	q := query + filter + offset + limit

	q, arr = helper.ReplaceQueryParams(q, params)
	rows, err := s.db.Query(ctx, q, arr...)
	if err != nil {
		return res, err
	}
	defer rows.Close()

	for rows.Next() {
		obj := &module.Region{}

		err = rows.Scan(
			&obj.Id,
			&obj.Name,
			&obj.RepublicId,
		)
		if err != nil {
			return res, err
		}

		res.Regions = append(res.Regions, *obj)
	}

	return res, nil
}
func (s *RegionRepo) Update(ctx context.Context, req *module.Region) (rowsAffected int64, err error) {

	nameInByte, err := json.Marshal(req.Name)

	if err != nil {
		return 0, err
	}

	query := `UPDATE "region" SET                
    	name = :name, republic_id = :republic_id
	WHERE
		id = :id`

	params := map[string]interface{}{
		"id":          req.Id,
		"name":        nameInByte,
		"republic_id": req.RepublicId,
	}

	q, arr := helper.ReplaceQueryParams(query, params)
	result, err := s.db.Exec(ctx, q, arr...)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
func (s *RegionRepo) Delete(ctx context.Context, req *module.GetByPKey) (rowsAffected int64, err error) {
	query := `DELETE FROM "region" WHERE id = $1`

	result, err := s.db.Exec(ctx, query, req.Id)
	if err != nil {
		return 0, err
	}

	rowsAffected = result.RowsAffected()

	return rowsAffected, err
}
