package service

import (
	"context"
	"errors"
	"fmt"
	"osg_intern/api/module"
	"osg_intern/config"
	"osg_intern/pkg/logger"
	"osg_intern/pkg/security"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type userService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) userService {
	return userService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s userService) CreateUser(ctx context.Context, req *module.CretaeUser) (res *module.User, err error) {
	s.log.Info("---CreateUser--->", logger.Any("req", req))

	if len(req.Password) < 6 {
		err := fmt.Errorf("password must not be less than 6 characters")
		s.log.Error("!!!CreateUser--->", logger.Error(err))
		return nil, err
	}

	hashedPassword, err := security.HashPassword(req.Password)
	if err != nil {
		s.log.Error("!!!CreateUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	req.Password = hashedPassword

	res, err = s.strg.User().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	_, err = s.strg.Role().Create(ctx, &module.Role{
		UserId:   res.Id,
		RoleType: res.Role,
	})

	if err != nil {
		s.log.Error("!!!CreateRole--->", logger.Error(err))
	}

	return res, nil
}

func (s userService) GetUser(ctx context.Context, req *module.GetByPKey) (res *module.User, err error) {
	s.log.Info("---GetUser--->", logger.Any("req", req))

	res, err = s.strg.User().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetUser--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	role, err := s.strg.Role().GetList(ctx, &module.RoleGetListReq{
		UserId: req.Id,
		Limit:  1,
	})
	if err != nil {
		s.log.Error("!!!GetRole--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	res.Role = role.Roles[0].RoleType

	return res, nil
}

func (s userService) GetUserList(ctx context.Context, req *module.GetListUserReq) (res *module.GetListUserResp, err error) {
	s.log.Info("---GetUserList--->", logger.Any("req", req))

	res, err = s.strg.User().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetUserList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s userService) UpdateUser(ctx context.Context, req *module.UpdateUser) (res *module.User, err error) {
	s.log.Info("---UpdateUser--->", logger.Any("req", req))

	user := module.User{
		Id:              req.Id,
		FullName:        req.FullName,
		BirthDate:       req.BirthDate,
		BirthDistrictId: req.BirthDistrictId,
		Avatar:          req.Avatar,
		Username:        req.Username,
		Role:            req.Role,
	}

	if req.Password != "" && req.NewPassword != "" && req.ConfirmPassword != "" {
		if req.NewPassword != req.ConfirmPassword {
			err := errors.New("invalid username or password")
			s.log.Error("!!!UpdateUser--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		if len(req.NewPassword) < 6 {
			err := fmt.Errorf("password must not be less than 6 characters")
			s.log.Error("!!!UpdateUser--->", logger.Error(err))
			return nil, err
		}

		hashedPassword, err := security.HashPassword(req.Password)
		if err != nil {
			s.log.Error("!!!UpdateUser--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		user, err := s.strg.User().GetUserByUsername(ctx, &module.GetUserByUserName{
			Username: req.Username,
		})

		if err != nil {
			err := errors.New("invalid username or password")
			s.log.Error("!!!UpdateUser--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		match, err := security.ComparePassword(user.Password, req.Password)
		if err != nil {
			s.log.Error("!!!UpdateUser--->", logger.Error(err))
			return nil, status.Error(codes.Internal, err.Error())
		}

		if !match {
			err := errors.New("username or password is wrong")
			s.log.Error("!!!UpdateUser--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		user.Password = hashedPassword
	}

	rowsAffected, err := s.strg.User().Update(ctx, &user)

	if err != nil {
		s.log.Error("!!!UpdateUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return &user, nil
}

func (s userService) DeleteUser(ctx context.Context, req *module.GetByPKey) (res string, err error) {
	s.log.Info("---DeleteUser--->", logger.Any("req", req))

	rowsAffected, err := s.strg.User().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteUser--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
