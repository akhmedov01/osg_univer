package service

import (
	"context"
	"osg_intern/config"

	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type positionService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewPositionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) positionService {
	return positionService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s positionService) CreatePosition(ctx context.Context, req *module.CreatePosition) (res *module.Position, err error) {
	s.log.Info("---CreatePosition--->", logger.Any("req", req))

	res, err = s.strg.Position().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreatePosition--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return res, nil
}

func (s positionService) GetPosition(ctx context.Context, req *module.GetByPKey) (res *module.Position, err error) {
	s.log.Info("---GetPosition--->", logger.Any("req", req))

	res, err = s.strg.Position().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetPosition--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return res, nil
}

func (s positionService) GetPositionList(ctx context.Context, req *module.GetListPositionReq) (res *module.GetListPositionRes, err error) {
	s.log.Info("---GetPositionList--->", logger.Any("req", req))

	res, err = s.strg.Position().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetPositionList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s positionService) UpdatePosition(ctx context.Context, req *module.Position) (res string, err error) {
	s.log.Info("---UpdatePosition--->", logger.Any("req", req))

	rowsAffected, err := s.strg.Position().Update(ctx, req)
	if err != nil {
		s.log.Error("!!!UpdatePosition--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return "Updated", nil
}

func (s positionService) DeletePosition(ctx context.Context, req *module.GetByPKey) (res string, err error) {
	s.log.Info("---DeletePosition--->", logger.Any("req", req))

	res = "Deleted"

	rowsAffected, err := s.strg.Position().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeletePosition--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
