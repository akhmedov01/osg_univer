package service

import (
	"context"
	"osg_intern/config"

	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type roleService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewRoleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) roleService {
	return roleService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s *roleService) CreateRole(ctx context.Context, req *module.Role) (res *module.Role, err error) {
	s.log.Info("---CreateRole--->", logger.Any("req", req))

	res, err = s.strg.Role().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateRole--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return res, nil
}

func (s *roleService) GetRole(ctx context.Context, req *module.PrimaryKeyUUID) (res *module.Role, err error) {
	s.log.Info("---GetRole--->", logger.Any("req", req))

	res, err = s.strg.Role().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetRole--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return res, nil
}

func (s *roleService) GetRoleList(ctx context.Context, req *module.RoleGetListReq) (res *module.GetRoleListRes, err error) {
	s.log.Info("---GetRoleList--->", logger.Any("req", req))

	res, err = s.strg.Role().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetRoleList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s *roleService) UpdateRole(ctx context.Context, req *module.Role) (res *module.Role, err error) {
	s.log.Info("---UpdateRole--->", logger.Any("req", req))

	rowsAffected, err := s.strg.Role().Update(ctx, req)
	if err != nil {
		s.log.Error("!!!UpdateRole--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}

func (s *roleService) DeleteRole(ctx context.Context, req *module.PrimaryKeyUUID) (res string, err error) {
	s.log.Info("---DeleteRole--->", logger.Any("req", req))

	res = ""

	rowsAffected, err := s.strg.Role().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteRole--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
