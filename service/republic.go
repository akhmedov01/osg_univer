package service

import (
	"context"
	"osg_intern/config"

	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type republicService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewRepublicService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) republicService {
	return republicService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s republicService) CreateRepublic(ctx context.Context, req *module.CreateRepublic) (res *module.Republic, err error) {
	s.log.Info("---CreateRepublic--->", logger.Any("req", req))

	res, err = s.strg.Republic().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateRepublic--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return res, nil
}

func (s republicService) GetRepublic(ctx context.Context, req *module.GetByPKey) (res *module.Republic, err error) {
	s.log.Info("---GetRepublic--->", logger.Any("req", req))

	res, err = s.strg.Republic().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetRepublic--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return res, nil
}

func (s republicService) GetRepublicList(ctx context.Context, req *module.GetListRepublicReq) (res *module.GetListRepublicRes, err error) {
	s.log.Info("---GetRepublicList--->", logger.Any("req", req))

	res, err = s.strg.Republic().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetRepublicList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s republicService) UpdateRepublic(ctx context.Context, req *module.Republic) (res string, err error) {
	s.log.Info("---UpdateRepublic--->", logger.Any("req", req))

	rowsAffected, err := s.strg.Republic().Update(ctx, req)
	if err != nil {
		s.log.Error("!!!UpdateRepublic--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return "Updated", nil
}

func (s republicService) DeleteRepublic(ctx context.Context, req *module.GetByPKey) (res string, err error) {
	s.log.Info("---DeleteRepublic--->", logger.Any("req", req))

	res = "Deleted"

	rowsAffected, err := s.strg.Republic().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteRepublic--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
