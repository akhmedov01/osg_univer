package service

import (
	"context"
	"osg_intern/config"

	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type districtService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewDistrictService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) districtService {
	return districtService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s districtService) CreateDistrict(ctx context.Context, req *module.CreateDistrict) (res *module.District, err error) {
	s.log.Info("---CreateDistrict--->", logger.Any("req", req))

	res, err = s.strg.District().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateDistrict--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return res, nil
}

func (s districtService) GetDistrict(ctx context.Context, req *module.GetByPKey) (res *module.District, err error) {
	s.log.Info("---GetDistrict--->", logger.Any("req", req))

	res, err = s.strg.District().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetDistrict--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return res, nil
}

func (s districtService) GetDistrictList(ctx context.Context, req *module.GetListDistrictReq) (res *module.GetListDistrictRes, err error) {
	s.log.Info("---GetDistrictList--->", logger.Any("req", req))

	res, err = s.strg.District().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetDistrictList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s districtService) UpdateDistrict(ctx context.Context, req *module.District) (res string, err error) {
	s.log.Info("---UpdateDistrict--->", logger.Any("req", req))

	rowsAffected, err := s.strg.District().Update(ctx, req)
	if err != nil {
		s.log.Error("!!!UpdateDistrict--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return "Updated", nil
}

func (s districtService) DeleteDistrict(ctx context.Context, req *module.GetByPKey) (res string, err error) {
	s.log.Info("---DeleteDistrict--->", logger.Any("req", req))

	res = "Deleted"

	rowsAffected, err := s.strg.District().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteDistrict--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
