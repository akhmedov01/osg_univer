package service

import (
	"context"
	"osg_intern/config"

	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type regionService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewRegionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) regionService {
	return regionService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s regionService) CreateRegion(ctx context.Context, req *module.CreateRegion) (res *module.Region, err error) {
	s.log.Info("---CreateRegion--->", logger.Any("req", req))

	res, err = s.strg.Region().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateRegion--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return res, nil
}

func (s regionService) GetRegion(ctx context.Context, req *module.GetByPKey) (res *module.Region, err error) {
	s.log.Info("---GetRegion--->", logger.Any("req", req))

	res, err = s.strg.Region().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetRegion--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return res, nil
}

func (s regionService) GetRegionList(ctx context.Context, req *module.GetListRegionReq) (res *module.GetListRegionRes, err error) {
	s.log.Info("---GetRegionList--->", logger.Any("req", req))

	res, err = s.strg.Region().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetRegionList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s regionService) UpdateRegion(ctx context.Context, req *module.Region) (res string, err error) {
	s.log.Info("---UpdateRegion--->", logger.Any("req", req))

	rowsAffected, err := s.strg.Region().Update(ctx, req)
	if err != nil {
		s.log.Error("!!!UpdateRegion--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return "Updated", nil
}

func (s regionService) DeleteRegion(ctx context.Context, req *module.GetByPKey) (res string, err error) {
	s.log.Info("---DeleteRegion--->", logger.Any("req", req))

	res = "Deleted"

	rowsAffected, err := s.strg.Region().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteRegion--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
