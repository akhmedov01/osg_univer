package service

import (
	"context"
	"osg_intern/config"

	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"osg_intern/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type departmentService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
}

func NewDepartmentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) departmentService {
	return departmentService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s departmentService) CreateDepartment(ctx context.Context, req *module.CreateDepartment) (res *module.Department, err error) {
	s.log.Info("---CreateDepartment--->", logger.Any("req", req))

	res, err = s.strg.Department().Create(ctx, req)
	if err != nil {
		s.log.Error("!!!CreateDepartment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return res, nil
}

func (s departmentService) GetDepartment(ctx context.Context, req *module.GetByPKey) (res *module.Department, err error) {
	s.log.Info("---GetDepartment--->", logger.Any("req", req))

	res, err = s.strg.Department().Get(ctx, req)
	if err != nil {
		s.log.Error("!!!GetDepartment--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return res, nil
}

func (s departmentService) GetDepartmentList(ctx context.Context, req *module.GetListDepartmentReq) (res *module.GetListDepartmentRes, err error) {
	s.log.Info("---GetDepartmentList--->", logger.Any("req", req))

	res, err = s.strg.Department().GetList(ctx, req)
	if err != nil {
		s.log.Error("!!!GetDepartmentList--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	return res, nil
}

func (s departmentService) UpdateDepartment(ctx context.Context, req *module.Department) (res string, err error) {
	s.log.Info("---UpdateDepartment--->", logger.Any("req", req))

	rowsAffected, err := s.strg.Department().Update(ctx, req)
	if err != nil {
		s.log.Error("!!!UpdateDepartment--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return "Deleted", nil
}

func (s departmentService) DeleteDepartment(ctx context.Context, req *module.GetByPKey) (res string, err error) {
	s.log.Info("---DeleteDepartment--->", logger.Any("req", req))

	res = "Deleted"

	rowsAffected, err := s.strg.Department().Delete(ctx, req)
	if err != nil {
		s.log.Error("!!!DeleteDepartment--->", logger.Error(err))
		return "", status.Error(codes.Internal, err.Error())
	}

	if rowsAffected <= 0 {
		return "", status.Error(codes.InvalidArgument, "no rows were affected")
	}

	return res, nil
}
