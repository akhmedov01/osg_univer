package service

import (
	"context"
	"errors"
	"osg_intern/api/module"
	"osg_intern/config"
	"osg_intern/pkg/logger"
	"osg_intern/pkg/security"

	"osg_intern/storage"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type sessionServiceServer struct {
	storage storage.StorageI
	log     logger.LoggerI
	cfg     config.Config
}

func NewSessionService(cfg config.Config, storage storage.StorageI, log logger.LoggerI) sessionServiceServer {
	return sessionServiceServer{
		storage: storage,
		log:     log,
		cfg:     cfg,
	}
}

func (s sessionServiceServer) Login(ctx context.Context, req *module.LoginReq) (*module.LoginRes, error) {
	s.log.Info("---Login--->", logger.Any("req", req))
	res := &module.LoginRes{}

	if len(req.Password) < 6 {
		err := errors.New("invalid password")
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	user, err := s.storage.User().GetUserByUsername(ctx, &module.GetUserByUserName{
		Username: req.Username,
	})
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		err := errors.New("invalid user data")
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	match, err := security.ComparePassword(user.Password, req.Password)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	if !match {
		err := errors.New("username or password is wrong")
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	res.UserFound = true
	res.User = *user

	roleTypes := ""

	if req.XRole == config.ROLE_EMPLOYEE {
		roleTypes = config.ROLE_EMPLOYEE
	} else if req.XRole == config.ROLE_STUDENT {
		roleTypes = config.ROLE_STUDENT
	} else {
		err := errors.New("not valid user type")
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	roles, err := s.storage.Role().GetList(
		ctx,
		&module.RoleGetListReq{ // @TODO limit offset
			UserId:   user.Id,
			RoleType: roleTypes,
		},
	)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	res.Role = roles.Roles

	if len(roles.Roles) == 0 {
		err := errors.New("permission denied")
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	} else if len(roles.Roles) == 1 {
		res.RoleId = roles.Roles[0].Id
	}

	s.log.Info("Login--->STRG: DeleteExpiredUserSessions", logger.Any("user_id", user.Id))
	rowsAffected, err := s.storage.Session().DeleteExpiredUserSessions(ctx, user.Id)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	s.log.Info("Login--->DeleteExpiredUserSessions", logger.Any("rowsAffected", rowsAffected))

	userSessionList, err := s.storage.Session().GetSessionListByUserID(ctx, user.Id)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	res.Sessions = userSessionList.Sessions

	sessionPKey, err := s.storage.Session().Create(ctx, &module.CreateSessionReq{
		UserId:    user.Id,
		RoleId:    res.RoleId,
		IP:        "0.0.0.0",
		ExpiredAt: time.Now().Add(config.RefreshTokenExpiresInTime).Format(config.DatabaseTimeLayout),
	})
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	session, err := s.storage.Session().GetByPK(ctx, &module.PrimaryKeyUUID{
		Id: sessionPKey.Id,
	})
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	m := map[string]interface{}{
		"id": session.Id,
	}

	accessToken, err := security.GenerateJWT(m, config.AccessTokenExpiresInTime, s.cfg.SecretKey)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	refreshToken, err := security.GenerateJWT(m, config.RefreshTokenExpiresInTime, s.cfg.SecretKey)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	res.Token = module.Token{
		AccessToken:      accessToken,
		RefreshToken:     refreshToken,
		CreatedAt:        session.CreatedAt,
		UpdatedAt:        session.UpdatedAt,
		ExpiredAt:        session.ExpiredAt,
		RefreshInSeconds: int(config.AccessTokenExpiresInTime.Seconds()),
	}

	return res, nil
}

func (s sessionServiceServer) Logout(ctx context.Context, req *module.LogoutReq) (string, error) {
	s.log.Info("---Logout--->", logger.Any("req", req))
	tokenInfo, err := security.ParseClaims(req.AccessToken, s.cfg.SecretKey)
	if err != nil {
		s.log.Error("!!!Logout--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	rowsAffected, err := s.storage.Session().Delete(ctx, &module.PrimaryKeyUUID{
		Id: tokenInfo.ID,
	})

	if err != nil {
		s.log.Error("!!!Logout--->", logger.Error(err))
		return "", status.Error(codes.InvalidArgument, err.Error())
	}

	s.log.Info("---Logout--->", logger.Any("tokenInfo", tokenInfo))
	s.log.Info("---Logout--->", logger.Any("rowsAffected", rowsAffected))

	return "", nil
}

func (s sessionServiceServer) RefreshToken(ctx context.Context, req *module.RefreshTokenReq) (*module.RefreshTokenRes, error) {
	res := &module.RefreshTokenRes{}

	tokenInfo, err := security.ParseClaims(req.RefreshToken, s.cfg.SecretKey)
	if err != nil {
		s.log.Error("!!!RefreshToken--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if tokenInfo.ExpiresAt.Unix() < time.Now().Unix() {
		err := errors.New("token has been expired")
		s.log.Error("!!!HasAccess--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	session := &module.Session{}

	session, err = s.storage.Session().GetByPK(ctx, &module.PrimaryKeyUUID{
		Id: tokenInfo.ID,
	})

	if err != nil {
		s.log.Error("!!!RefreshToken--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = s.storage.Session().Update(ctx, &module.UpdateSessionReq{
		Id:        session.Id,
		UserId:    session.UserId,
		RoleId:    session.RoleId,
		IP:        session.IP,
		ExpiredAt: session.ExpiredAt,
	})
	if err != nil {
		s.log.Error("!!!RefreshToken--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = s.storage.User().Get(ctx, &module.GetByPKey{
		Id: session.UserId,
	})

	if err != nil {
		s.log.Error("!!!RefreshToken--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	m := map[string]interface{}{
		"id": session.Id,
	}

	accessToken, err := security.GenerateJWT(m, config.AccessTokenExpiresInTime, s.cfg.SecretKey)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	refreshToken, err := security.GenerateJWT(m, config.RefreshTokenExpiresInTime, s.cfg.SecretKey)
	if err != nil {
		s.log.Error("!!!Login--->", logger.Error(err))
		return nil, status.Error(codes.Internal, err.Error())
	}

	res.Token = module.Token{
		AccessToken:      accessToken,
		RefreshToken:     refreshToken,
		CreatedAt:        session.CreatedAt,
		UpdatedAt:        session.UpdatedAt,
		ExpiredAt:        session.ExpiredAt,
		RefreshInSeconds: int(config.AccessTokenExpiresInTime.Seconds()),
	}

	return res, nil
}

// func (s *sessionService) HasAccess(ctx context.Context, req *module.HasAccessReq) (*module.HasAccessRes, error) {
// 	tokenInfo, err := security.ParseClaims(req.AccessToken, s.cfg.SecretKey)
// 	if err != nil {
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if tokenInfo.ExpiresAt.Unix() < time.Now().Unix() {
// 		err := errors.New("token has been expired")
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	session, err := s.strg.Auth().Session().GetByPK(ctx, &module.SessionPrimaryKey{
// 		Id: tokenInfo.ID,
// 	})
// 	if err != nil {
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}
// 	fmt.Println(session)

// 	if !util.IsValidUUID(session.GetRoleId()) {
// 		err := errors.New("not valid session role")
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	user, err := s.strg.Auth().User().Get(ctx, &module.GetUserReq{
// 		Id: session.UserId,
// 	})
// 	if err != nil {
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if !user.EmailVerification {
// 		err := errors.New("email hasn't been activated yet")
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	//_, err = s.strg.Scope().Upsert(ctx, &module.UpsertScopeRequest{
// 	//	ClientPlatformId: req.ClientPlatformId,
// 	//	Path:             req.Path,
// 	//	Method:           req.Method,
// 	//})
// 	//if err != nil {
// 	//	s.log.Error("!!!HasAccess--->", logger.Error(err))
// 	//	return nil, status.Error(codes.Internal, err.Error())
// 	//}

// 	// DONT FORGET TO UNCOMMENT THIS!!!

// 	hasAccess, err := s.storage.PermissionScope().HasAccess(ctx, session.RoleId, req.Path, req.Method)

// 	if err != nil {
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	if !hasAccess {
// 		err = errors.New("access denied")
// 		s.log.Error("!!!HasAccess--->", logger.Error(err))
// 		return nil, status.Error(codes.InvalidArgument, err.Error())
// 	}

// 	return module.HasAccessRes{
// 		Id:        session.Id,
// 		UserId:    session.UserId,
// 		RoleId:    session.RoleId,
// 		Ip:        session.Ip,
// 		Data:      session.Data,
// 		ExpiresAt: session.ExpiresAt,
// 		CreatedAt: session.CreatedAt,
// 		UpdatedAt: session.UpdatedAt,
// 	}, nil
// }
