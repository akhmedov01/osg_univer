package service

import (
	"osg_intern/config"
	"osg_intern/pkg/logger"
	"osg_intern/storage"
)

type IServiceManager interface {
	SessionService() sessionServiceServer
	RoleService() roleService
	UserService() userService
	DepartmentService() departmentService
	DistrictService() districtService
	PositionService() positionService
	RegionService() regionService
	RepublicService() republicService
}

type Service struct {
	sessionService    sessionServiceServer
	roleService       roleService
	userService       userService
	departmentService departmentService
	districtService   districtService
	positionService   positionService
	regionService     regionService
	republicService   republicService
}

func New(cfg config.Config, storage storage.StorageI, log logger.LoggerI) Service {
	services := Service{}

	services.sessionService = NewSessionService(cfg, storage, log)
	services.roleService = NewRoleService(cfg, log, storage)
	services.userService = NewUserService(cfg, log, storage)
	services.departmentService = NewDepartmentService(cfg, log, storage)
	services.districtService = NewDistrictService(cfg, log, storage)
	services.positionService = NewPositionService(cfg, log, storage)
	services.regionService = NewRegionService(cfg, log, storage)
	services.republicService = NewRepublicService(cfg, log, storage)

	return services
}

// SessionService implements IServiceManager.
func (g Service) SessionService() sessionServiceServer {
	return g.sessionService
}

func (g Service) RepublicService() republicService {
	return g.republicService
}

func (g Service) RoleService() roleService {
	return g.roleService
}

func (g Service) UserService() userService {
	return g.userService
}

func (g Service) DepartmentService() departmentService {
	return g.departmentService
}

func (g Service) DistrictService() districtService {
	return g.districtService
}

func (g Service) PositionService() positionService {
	return g.positionService
}

func (g Service) RegionService() regionService {
	return g.regionService
}
