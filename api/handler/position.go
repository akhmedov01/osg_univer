package handler

import (
	"context"
	"fmt"
	"osg_intern/api/http"
	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreatePosition godoc
// @Router       /position [POST]
// @Summary      Create Position
// @Description  Create Position
// @Tags         POSITION
// @Accept       json
// @Produce      json
// @Param        data  body      module.CreatePosition  true  "Position data"
// @Success      200  {string}  string
// @Failure      400  {object}  http.ErrorResp
// @Failure      404  {object}  http.ErrorResp
// @Failure      500  {object}  http.ErrorResp
func (h *Handler) CreatePosition(c *gin.Context) {

	var position module.CreatePosition
	err := c.ShouldBindJSON(&position)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	resp, err := h.services.PositionService().CreatePosition(context.Background(), &module.CreatePosition{
		Name: position.Name,
	})

	if err != nil {
		fmt.Println("error Position Create:", err.Error())
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetPositionList godoc
// @ID get_position_list
// @Router /position [GET]
// @Summary Get Position List
// @Description  Get Position List
// @Tags POSITION
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=module.GetListPositionRes} "GetPositionListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPositionList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Position().GetList(
		c.Request.Context(),
		&module.GetListPositionReq{
			Limit:  limit,
			Page:   offset,
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetPositionByID godoc
// @ID get_position_by_id
// @Router /position/{id}/ [GET]
// @Summary Get Position By ID
// @Description Get Position By ID
// @Tags POSITION
// @Accept json
// @Produce json
// @Param id path int true "Position-id"
// @Success 200 {object} http.Response{data=module.Position} "PositionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetPositionByID(c *gin.Context) {

	i := c.Param("id")

	positionID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Position().Get(
		c.Request.Context(),
		&module.GetByPKey{
			Id: positionID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdatePosition godoc
// @ID update_position
// @Router /position [PUT]
// @Summary Update Position
// @Description Update Position
// @Tags POSITION
// @Accept json
// @Produce json
// @Param position body module.Position true "UpdatePositionRequestBody"
// @Success 200 {object} http.Response{data=string} "Position data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdatePosition(c *gin.Context) {
	var position module.Position

	err := c.ShouldBindJSON(&position)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.strg.Position().Update(
		c.Request.Context(),
		&position,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeletePosition godoc
// @ID delete_position
// @Router /position/{id} [DELETE]
// @Summary Delete Position
// @Description Delate Position
// @Tags POSITION
// @Accept json
// @Produce json
// @Param id path int true "Position-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeletePosition(c *gin.Context) {
	i := c.Param("id")

	positionID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	_, err = h.strg.Position().Delete(
		c.Request.Context(),
		&module.GetByPKey{
			Id: positionID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, "")
}
