package handler

import (
	"context"
	"fmt"
	"osg_intern/api/http"
	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateRepublic godoc
// @Router       /republic [POST]
// @Summary      Create Republic
// @Description  Create Republic
// @Tags         REPUBLIC
// @Accept       json
// @Produce      json
// @Param        data  body      module.CreateRepublic  true  "Republic data"
// @Success      200  {string}  string
// @Failure      400  {object}  http.ErrorResp
// @Failure      404  {object}  http.ErrorResp
// @Failure      500  {object}  http.ErrorResp
func (h *Handler) CreateRepublic(c *gin.Context) {

	var republic module.CreateRepublic
	err := c.ShouldBindJSON(&republic)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	resp, err := h.services.RepublicService().CreateRepublic(context.Background(), &module.CreateRepublic{
		Name: republic.Name,
	})

	if err != nil {
		fmt.Println("error Republic Create:", err.Error())
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetRepublicList godoc
// @ID get_republic_list
// @Router /republic [GET]
// @Summary Get Republic List
// @Description  Get Republic List
// @Tags REPUBLIC
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=module.GetListRepublicRes} "GetRepublicListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRepublicList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Republic().GetList(
		c.Request.Context(),
		&module.GetListRepublicReq{
			Limit:  limit,
			Page:   offset,
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetRepublicByID godoc
// @ID get_republic_by_id
// @Router /republic/{id}/ [GET]
// @Summary Get Republic By ID
// @Description Get Republic By ID
// @Tags REPUBLIC
// @Accept json
// @Produce json
// @Param id path int true "Republic-id"
// @Success 200 {object} http.Response{data=module.Republic} "RepublicBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRepublicByID(c *gin.Context) {

	i := c.Param("id")

	republicID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	fmt.Printf("republicID: %v ", republicID)

	resp, err := h.strg.Republic().Get(
		c.Request.Context(),
		&module.GetByPKey{
			Id: republicID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateRepublic godoc
// @ID update_republic
// @Router /republic [PUT]
// @Summary Update Republic
// @Description Update Republic
// @Tags REPUBLIC
// @Accept json
// @Produce json
// @Param republic body module.Republic true "UpdateRepublicRequestBody"
// @Success 200 {object} http.Response{data=string} "Republic data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateRepublic(c *gin.Context) {
	var republic module.Republic

	err := c.ShouldBindJSON(&republic)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.strg.Republic().Update(
		c.Request.Context(),
		&republic,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteRepublic godoc
// @ID delete_republic
// @Router /republic/{id} [DELETE]
// @Summary Delete Republic
// @Description Delate Republic
// @Tags REPUBLIC
// @Accept json
// @Produce json
// @Param id path int true "Republic-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteRepublic(c *gin.Context) {
	i := c.Param("id")

	republicID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	_, err = h.strg.Republic().Delete(
		c.Request.Context(),
		&module.GetByPKey{
			Id: republicID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, "")
}
