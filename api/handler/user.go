package handler

import (
	"context"
	"fmt"
	"osg_intern/api/http"
	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateUser godoc
// @Router       /users [POST]
// @Summary      Create User
// @Description  Create User
// @Tags         USER
// @Accept       json
// @Produce      json
// @Param        data  body      module.CretaeUser  true  "User data"
// @Success      200  {string}  string
// @Failure      400  {object}  http.ErrorResp
// @Failure      404  {object}  http.ErrorResp
// @Failure      500  {object}  http.ErrorResp
func (h *Handler) CreateUser(c *gin.Context) {

	var user module.CretaeUser
	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	resp, err := h.services.UserService().CreateUser(context.Background(), &module.CretaeUser{
		Username:        user.Username,
		Password:        user.Password,
		FullName:        user.FullName,
		BirthDate:       user.BirthDate,
		Avatar:          user.Avatar,
		BirthDistrictId: user.BirthDistrictId,
	})

	if err != nil {
		fmt.Println("error User Create:", err.Error())
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetUserList godoc
// @ID get_user_list
// @Router /users [GET]
// @Summary Get User List
// @Description  Get User List
// @Tags USER
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=module.GetListUserResp} "GetUserListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.User().GetList(
		c.Request.Context(),
		&module.GetListUserReq{
			Limit:  limit,
			Page:   offset,
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetUserByID godoc
// @ID get_user_by_id
// @Router /users/{id}/ [GET]
// @Summary Get User By ID
// @Description Get User By ID
// @Tags USER
// @Accept json
// @Produce json
// @Param id path int true "user-id"
// @Success 200 {object} http.Response{data=module.User} "UserBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetUserByID(c *gin.Context) {

	i := c.Param("id")

	userID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}
	resp, err := h.strg.User().Get(
		c.Request.Context(),
		&module.GetByPKey{
			Id: userID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateUser godoc
// @ID update_user
// @Router /users [PUT]
// @Summary Update User
// @Description Update User
// @Tags USER
// @Accept json
// @Produce json
// @Param user body module.User true "UpdateUserRequestBody"
// @Success 200 {object} http.Response{data=string} "User data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateUser(c *gin.Context) {
	var user module.User

	err := c.ShouldBindJSON(&user)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.strg.User().Update(
		c.Request.Context(),
		&user,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteUser godoc
// @ID delete_user
// @Router /users/{id} [DELETE]
// @Summary Delete user
// @Description Get user
// @Tags USER
// @Accept json
// @Produce json
// @Param id path int true "user-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteUser(c *gin.Context) {
	i := c.Param("id")

	userID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	_, err = h.strg.User().Delete(
		c.Request.Context(),
		&module.GetByPKey{
			Id: userID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, "")
}
