package handler

import (
	"context"
	"fmt"
	"osg_intern/api/http"
	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateDepartment godoc
// @Router       /department [POST]
// @Summary      Create Department
// @Description  Create Department
// @Tags         DEPARTMENT
// @Accept       json
// @Produce      json
// @Param        data  body      module.CreateDepartment  true  "Department data"
// @Success      200  {string}  string
// @Failure      400  {object}  http.ErrorResp
// @Failure      404  {object}  http.ErrorResp
// @Failure      500  {object}  http.ErrorResp
func (h *Handler) CreateDepartment(c *gin.Context) {

	var department module.CreateDepartment
	err := c.ShouldBindJSON(&department)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	resp, err := h.services.DepartmentService().CreateDepartment(context.Background(), &module.CreateDepartment{
		Name: department.Name,
	})

	if err != nil {
		fmt.Println("error Department Create:", err.Error())
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetDepartmentList godoc
// @ID get_Department_list
// @Router /department [GET]
// @Summary Get Department List
// @Description  Get Department List
// @Tags DEPARTMENT
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=module.GetListDepartmentRes} "GetDepartmentListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDepartmentList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Department().GetList(
		c.Request.Context(),
		&module.GetListDepartmentReq{
			Limit:  limit,
			Page:   offset,
			Search: c.Query("search"),
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetDepartmentByID godoc
// @ID get_department_by_id
// @Router /department/{id}/ [GET]
// @Summary Get Department By ID
// @Description Get Department By ID
// @Tags DEPARTMENT
// @Accept json
// @Produce json
// @Param id path int true "department-id"
// @Success 200 {object} http.Response{data=module.Department} "DepartmentBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDepartmentByID(c *gin.Context) {

	i := c.Param("id")

	departmentID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Department().Get(
		c.Request.Context(),
		&module.GetByPKey{
			Id: departmentID,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateDepartment godoc
// @ID update_department
// @Router /department [PUT]
// @Summary Update Department
// @Description Update Department
// @Tags DEPARTMENT
// @Accept json
// @Produce json
// @Param department body module.Department true "UpdateDepartmentRequestBody"
// @Success 200 {object} http.Response{data=string} "Department data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateDepartment(c *gin.Context) {
	var department module.Department

	err := c.ShouldBindJSON(&department)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.strg.Department().Update(
		c.Request.Context(),
		&department,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteDepartment godoc
// @ID delete_department
// @Router /department/{id} [DELETE]
// @Summary Delete Department
// @Description Delate Department
// @Tags DEPARTMENT
// @Accept json
// @Produce json
// @Param id path int true "Department-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteDepartment(c *gin.Context) {
	i := c.Param("id")

	departmentID, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	_, err = h.strg.Department().Delete(
		c.Request.Context(),
		&module.GetByPKey{
			Id: departmentID,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, "")
}
