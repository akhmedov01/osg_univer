package handler

import (
	"osg_intern/api/http"
	"osg_intern/config"
	"osg_intern/pkg/logger"
	"osg_intern/service"
	"osg_intern/storage"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	strg     storage.StorageI
	cfg      config.Config
	log      logger.LoggerI
	services service.IServiceManager
}

func NewHandler(strg storage.StorageI, conf config.Config, loger logger.LoggerI, service service.IServiceManager) *Handler {
	return &Handler{strg: strg, cfg: conf, log: loger, services: service}
}

func (h *Handler) handleResponse(c *gin.Context, status http.Status, data interface{}) {
	switch code := status.Code; {
	case code < 300:
		h.log.Info(
			"---Response--->",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	case code < 400:
		h.log.Warn(
			"!!!Response--->",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	default:
		h.log.Error(
			"!!!Response--->",
			logger.Int("code", status.Code),
			logger.String("status", status.Status),
			logger.Any("description", status.Description),
			logger.Any("data", data),
		)
	}

	c.JSON(status.Code, http.Response{
		Status:      status.Status,
		Description: status.Description,
		Data:        data,
	})
}

func (h *Handler) getOffsetParam(c *gin.Context) (offset int, err error) {
	offsetStr := c.DefaultQuery("offset", h.cfg.DefaultOffset)
	return strconv.Atoi(offsetStr)
}

func (h *Handler) getIdParam(c *gin.Context) (id int, err error) {
	idStr := c.DefaultQuery("id", h.cfg.DefaultOffset)
	return strconv.Atoi(idStr)
}

func (h *Handler) getLimitParam(c *gin.Context) (offset int, err error) {
	offsetStr := c.DefaultQuery("limit", h.cfg.DefaultLimit)
	return strconv.Atoi(offsetStr)
}
