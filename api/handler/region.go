package handler

import (
	"context"
	"fmt"
	"osg_intern/api/http"
	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateRegion godoc
// @Router       /region [POST]
// @Summary      Create Region
// @Description  Create Region
// @Tags         REGION
// @Accept       json
// @Produce      json
// @Param        data  body      module.CreateRegion  true  "Region data"
// @Success      200  {string}  string
// @Failure      400  {object}  http.ErrorResp
// @Failure      404  {object}  http.ErrorResp
// @Failure      500  {object}  http.ErrorResp
func (h *Handler) CreateRegion(c *gin.Context) {

	var region module.CreateRegion
	err := c.ShouldBindJSON(&region)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	resp, err := h.services.RegionService().CreateRegion(context.Background(), &module.CreateRegion{
		Name:       region.Name,
		RepublicId: region.RepublicId,
	})

	if err != nil {
		fmt.Println("error Region Create:", err.Error())
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetRegionList godoc
// @ID get_region_list
// @Router /region [GET]
// @Summary Get Region List
// @Description  Get Region List
// @Tags REGION
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param id query integer false "republic_id"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=module.GetListRegionRes} "GetRegionListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRegionList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	republicId, err := h.getIdParam(c)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Region().GetList(
		c.Request.Context(),
		&module.GetListRegionReq{
			Limit:      limit,
			Page:       offset,
			Search:     c.Query("search"),
			RepublicId: republicId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetRegionByID godoc
// @ID get_region_by_id
// @Router /region/{id}/ [GET]
// @Summary Get Region By ID
// @Description Get Region By ID
// @Tags REGION
// @Accept json
// @Produce json
// @Param id path int true "Region-id"
// @Success 200 {object} http.Response{data=module.Region} "RegionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetRegionByID(c *gin.Context) {

	i := c.Param("id")

	regionId, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.Region().Get(
		c.Request.Context(),
		&module.GetByPKey{
			Id: regionId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateRegion godoc
// @ID update_region
// @Router /region [PUT]
// @Summary Update Region
// @Description Update Region
// @Tags REGION
// @Accept json
// @Produce json
// @Param region body module.Region true "UpdateRegionRequestBody"
// @Success 200 {object} http.Response{data=string} "Region data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateRegion(c *gin.Context) {
	var region module.Region

	err := c.ShouldBindJSON(&region)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.strg.Region().Update(
		c.Request.Context(),
		&region,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteRegion godoc
// @ID delete_region
// @Router /region/{id} [DELETE]
// @Summary Delete Region
// @Description Delate Region
// @Tags REGION
// @Accept json
// @Produce json
// @Param id path int true "Region-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteRegion(c *gin.Context) {
	i := c.Param("id")

	regionId, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	_, err = h.strg.Region().Delete(
		c.Request.Context(),
		&module.GetByPKey{
			Id: regionId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, "")
}
