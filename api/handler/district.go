package handler

import (
	"context"
	"fmt"
	"osg_intern/api/http"
	"osg_intern/api/module"
	"osg_intern/pkg/logger"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateDistrict godoc
// @Router       /district [POST]
// @Summary      Create District
// @Description  Create District
// @Tags         DISTRICT
// @Accept       json
// @Produce      json
// @Param        data  body      module.CreateDistrict  true  "District data"
// @Success      200  {string}  string
// @Failure      400  {object}  http.ErrorResp
// @Failure      404  {object}  http.ErrorResp
// @Failure      500  {object}  http.ErrorResp
func (h *Handler) CreateDistrict(c *gin.Context) {

	var district module.CreateDistrict
	err := c.ShouldBindJSON(&district)
	if err != nil {
		h.log.Error("error while binding:", logger.Error(err))
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	resp, err := h.services.DistrictService().CreateDistrict(context.Background(), &module.CreateDistrict{
		Name:     district.Name,
		RegionId: district.RegionId,
	})

	if err != nil {
		fmt.Println("error District Create:", err.Error())
		h.handleResponse(c, http.InternalServerError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetDistrictList godoc
// @ID get_district_list
// @Router /district [GET]
// @Summary Get District List
// @Description  Get District List
// @Tags DISTRICT
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param id query integer false "region_id"
// @Param search query string false "search"
// @Success 200 {object} http.Response{data=module.GetListDistrictRes} "GetDistrictListResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDistrictList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	regionId, err := h.getIdParam(c)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.District().GetList(
		c.Request.Context(),
		&module.GetListDistrictReq{
			Limit:    limit,
			Page:     offset,
			Search:   c.Query("search"),
			RegionId: regionId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// GetDistrictByID godoc
// @ID get_district_by_id
// @Router /district/{id}/ [GET]
// @Summary Get District By ID
// @Description Get District By ID
// @Tags DISTRICT
// @Accept json
// @Produce json
// @Param id path int true "District-id"
// @Success 200 {object} http.Response{data=module.District} "DistrictBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDistrictByID(c *gin.Context) {

	i := c.Param("id")

	districtId, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	resp, err := h.strg.District().Get(
		c.Request.Context(),
		&module.GetByPKey{
			Id: districtId,
		},
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// UpdateDistrict godoc
// @ID update_district
// @Router /district [PUT]
// @Summary Update District
// @Description Update District
// @Tags DISTRICT
// @Accept json
// @Produce json
// @Param district body module.District true "UpdateDistrictRequestBody"
// @Success 200 {object} http.Response{data=string} "District data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateDistrict(c *gin.Context) {
	var district module.District

	err := c.ShouldBindJSON(&district)
	if err != nil {
		h.handleResponse(c, http.BadRequest, err.Error())
		return
	}

	resp, err := h.strg.District().Update(
		c.Request.Context(),
		&district,
	)

	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.OK, resp)
}

// DeleteDistrict godoc
// @ID delete_district
// @Router /district/{id} [DELETE]
// @Summary Delete District
// @Description Delate District
// @Tags DISTRICT
// @Accept json
// @Produce json
// @Param id path int true "District-id"
// @Success 204
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteDistrict(c *gin.Context) {
	i := c.Param("id")

	districtId, err := strconv.Atoi(i)

	if err != nil {
		h.handleResponse(c, http.InvalidArgument, err.Error())
		return
	}

	_, err = h.strg.District().Delete(
		c.Request.Context(),
		&module.GetByPKey{
			Id: districtId,
		},
	)
	if err != nil {
		h.handleResponse(c, http.GRPCError, err.Error())
		return
	}

	h.handleResponse(c, http.NoContent, "")
}
