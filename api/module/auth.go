package module

type Session struct {
	Id        string `json:"id"`
	UserId    int    `json:"user_id"`
	RoleId    string `json:"role_id"`
	IP        string `json:"ip"`
	ExpiredAt string `json:"expired_at"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type SessionGetListReq struct {
	Page   int
	Limit  int
	Search string
}

type GetSessionListRes struct {
	Sessions []Session
	Count    int
}

type CreateSessionReq struct {
	UserId    int    `json:"user_id"`
	RoleId    string `json:"role_id"`
	IP        string `json:"ip"`
	ExpiredAt string `json:"expired_at"`
}

type UpdateSessionReq struct {
	Id        string `json:"id"`
	UserId    int    `json:"user_id"`
	RoleId    string `json:"role_id"`
	IP        string `json:"ip"`
	ExpiredAt string `json:"expired_at"`
}

type SessionAndTokenReq struct {
	LoginDate LoginRes
}

type LoginReq struct {
	Username string `json:"user_name"`
	Password string `json:"password"`
	XRole    string `json:"x_role"`
}

type LoginRes struct {
	UserFound bool      `json:"user_found"`
	User      User      `json:"user"`
	RoleId    string    `json:"role_id"`
	Token     Token     `json:"token"`
	Sessions  []Session `json:"sessions"`
	Role      []Role    `json:"roles"`
}

type LogoutReq struct {
	AccessToken string `json:"access_token"`
}

type RefreshTokenReq struct {
	RefreshToken string `json:"refresh_token"`
	UserId       int    `json:"user_id"`
}

type RefreshTokenRes struct {
	Token Token `json:"token"`
}

type Token struct {
	AccessToken      string `json:"access_token"`
	RefreshToken     string `json:"refresh_token"`
	CreatedAt        string `json:"created_at"`
	UpdatedAt        string `json:"updated_at"`
	ExpiredAt        string `json:"expired_at"`
	RefreshInSeconds int    `json:"refresh_in_seconds"`
}

type PrimaryKeyUUID struct {
	Id string
}
