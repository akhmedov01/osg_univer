package module

type Department struct {
	Id   int               `json:"id"`
	Name NameMultiLanguage `json:"name"`
}

type CreateDepartment struct {
	Name NameMultiLanguage `json:"name"`
}

type GetListDepartmentReq struct {
	Page   int    `json:"page"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
}

type GetListDepartmentRes struct {
	Departments []Department `json:"departments"`
	Count       int          `json:"count"`
}
