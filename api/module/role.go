package module

type Role struct {
	Id       string `json:"id"`
	UserId   int    `json:"user_id"`
	RoleType string `json:"role_type"`
}

type GetRoleListRes struct {
	Roles []Role
	Count int
}

type RoleGetListReq struct {
	Page     int
	Limit    int
	Search   string
	UserId   int    `json:"user_id"`
	RoleType string `json:"role_type"`
}
