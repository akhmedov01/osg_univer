package module

type Region struct {
	Id         int               `json:"id"`
	Name       NameMultiLanguage `json:"name"`
	RepublicId int               `json:"republic_id"`
}

type CreateRegion struct {
	Name       NameMultiLanguage `json:"name"`
	RepublicId int               `json:"republic_id"`
}

type GetListRegionReq struct {
	Page       int    `json:"page"`
	Limit      int    `json:"limit"`
	Search     string `json:"search"`
	RepublicId int    `json:"republic_id"`
}

type GetListRegionRes struct {
	Regions []Region `json:"regions"`
	Count   int      `json:"count"`
}
