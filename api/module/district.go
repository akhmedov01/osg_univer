package module

type District struct {
	Id       int               `json:"id"`
	Name     NameMultiLanguage `json:"name"`
	RegionId int               `json:"region_id"`
}

type CreateDistrict struct {
	Name     NameMultiLanguage `json:"name"`
	RegionId int               `json:"region_id"`
}

type GetListDistrictReq struct {
	Page     int    `json:"page"`
	Limit    int    `json:"limit"`
	Search   string `json:"search"`
	RegionId int    `json:"region_id"`
}

type GetListDistrictRes struct {
	Districts []District `json:"districts"`
	Count     int        `json:"count"`
}
