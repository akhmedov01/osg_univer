package module

type Republic struct {
	Id   int               `json:"id"`
	Name NameMultiLanguage `json:"name"`
}

type NameMultiLanguage struct {
	NameRu string `json:"name_ru"`
	NameUz string `json:"name_uz"`
	NameEn string `json:"name_en"`
}

type CreateRepublic struct {
	Name NameMultiLanguage `json:"name"`
}

type GetListRepublicReq struct {
	Page   int    `json:"page"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
}

type GetListRepublicRes struct {
	Republics []Republic `json:"republics"`
	Count     int        `json:"count"`
}
