package module

type Position struct {
	Id   int               `json:"id"`
	Name NameMultiLanguage `json:"name"`
}

type CreatePosition struct {
	Name NameMultiLanguage `json:"name"`
}

type GetListPositionReq struct {
	Page   int    `json:"page"`
	Limit  int    `json:"limit"`
	Search string `json:"search"`
}

type GetListPositionRes struct {
	Positions []Position `json:"positions"`
	Count     int        `json:"count"`
}
