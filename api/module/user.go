package module

type User struct {
	Id              int    `json:"id"`
	Username        string `json:"username"`
	Password        string `json:"password"`
	FullName        string `json:"full_name"`
	BirthDate       string `json:"birth_date"`
	Avatar          string `json:"avatar"`
	BirthDistrictId int    `json:"birth_district_id"`
	Role            string `json:"role"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
}

type CretaeUser struct {
	Username        string `json:"username"`
	Password        string `json:"password"`
	FullName        string `json:"full_name"`
	BirthDate       string `json:"birth_date"`
	Avatar          string `json:"avatar"`
	BirthDistrictId int    `json:"birth_district_id"`
	// Role            string `json:"role"`
}

type UpdateUser struct {
	Id              int    `json:"id"`
	Username        string `json:"username"`
	Password        string `json:"password"`
	FullName        string `json:"full_name"`
	BirthDate       string `json:"birth_date"`
	Avatar          string `json:"avatar"`
	BirthDistrictId int    `json:"birth_district_id"`
	Role            string `json:"role"`
	NewPassword     string `json:"new_password"`
	ConfirmPassword string `json:"confirm_password"`
}

type GetListUserReq struct {
	Page   int
	Limit  int
	Search string
}

type GetListUserResp struct {
	Users []User
	Count int
}

type GetByPKey struct {
	Id int
}

type GetUserByUserName struct {
	Username string
}
