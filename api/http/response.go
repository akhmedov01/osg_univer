package http

// Response ...
type Response struct {
	Status      string      `json:"status"`
	Description string      `json:"description"`
	Data        interface{} `json:"data"`
}

type ErrorResp struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

type CreateResponse struct {
	Id string `json:"id"`
}
