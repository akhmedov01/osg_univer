package api

import (
	_ "osg_intern/api/docs"
	"osg_intern/api/handler"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

func NewServer(h *handler.Handler) *gin.Engine {
	r := gin.Default()

	// USER

	r.POST("/users", h.CreateUser)
	r.GET("/users", h.GetUserList)
	r.GET("/users/:id", h.GetUserByID)
	r.PUT("/users", h.UpdateUser)
	r.DELETE("/users/:id", h.DeleteUser)

	// DEPARTMENT

	r.POST("/department", h.CreateDepartment)
	r.GET("/department", h.GetDepartmentList)
	r.GET("/department/:id", h.GetDepartmentByID)
	r.PUT("/department", h.UpdateDepartment)
	r.DELETE("/department/:id", h.DeleteDepartment)

	// DISTRICT

	r.POST("/district", h.CreateDistrict)
	r.GET("/district", h.GetDistrictList)
	r.GET("/district/:id", h.GetDistrictByID)
	r.PUT("/district", h.UpdateDistrict)
	r.DELETE("/district/:id", h.DeleteDistrict)

	// POSITION

	r.POST("/position", h.CreatePosition)
	r.GET("/position", h.GetPositionList)
	r.GET("/position/:id", h.GetPositionByID)
	r.PUT("/position", h.UpdatePosition)
	r.DELETE("/position/:id", h.DeletePosition)

	// REGION

	r.POST("/region", h.CreateRegion)
	r.GET("/region", h.GetRegionList)
	r.GET("/region/:id", h.GetRegionByID)
	r.PUT("/region", h.UpdateRegion)
	r.DELETE("/region/:id", h.DeleteRegion)

	// REPUBLIC

	r.POST("/republic", h.CreateRepublic)
	r.GET("/republic", h.GetRepublicList)
	r.GET("/republic/:id", h.GetRepublicByID)
	r.PUT("/republic", h.UpdateRepublic)
	r.DELETE("/republic/:id", h.DeleteRepublic)

	// REGISTRATION

	r.POST("/login", h.Login)
	r.DELETE("/logout", h.Logout)
	r.PUT("/refresh", h.RefreshToken)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return r
}
