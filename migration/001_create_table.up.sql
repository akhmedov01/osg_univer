CREATE TYPE "role_type" AS ENUM (
    'EMPLOYEE',
    'STUDENT'
);

CREATE TABLE "role" (
    "id" uuid PRIMARY KEY,
    "role_type" role_type,
    "user_id" integer,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE "session" (
    "id" uuid PRIMARY KEY,
    "user_id" integer,
    "role_id" uuid,
    "ip" inet,
    "expires_at" timestamp,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

-- CREATE TABLE IF NOT EXISTS "permission" (
--     "id" UUID PRIMARY KEY,
--     "parent_id" UUID,
--     "name" VARCHAR NOT NULL,
--     "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
--     "updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
--     UNIQUE ("parent_id", "name")
-- );

-- CREATE TABLE IF NOT EXISTS "permission_scope" (
--     "permission_id" UUID REFERENCES "permission"("id"),
--     "path" VARCHAR,
--     "method" VARCHAR,
--     "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
--     "updated_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
--     PRIMARY KEY ("permission_id", "path", "method")
-- );

-- CREATE TABLE IF NOT EXISTS "role_permission" (
--     "role_id" UUID REFERENCES "role"("id"),
--     "permission_id" UUID REFERENCES "permission"("id"),
--     "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
--     PRIMARY KEY ("role_id", "permission_id")
-- );

CREATE TABLE "user" (
    "id" serial PRIMARY KEY,
    "username" varchar,
    "password" varchar,
    "role" role_type,
    "full_name" varchar,
    "birth_date" date,
    "avatar" varchar,
    "birth_district_id" integer,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE "republic" (
    "id" serial PRIMARY KEY,
    "name" jsonb,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE "region" (
    "id" serial PRIMARY KEY,
    "name" jsonb,
    "republic_id" integer,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE "district" (
    "id" serial PRIMARY KEY,
    "name" jsonb,
    "region_id" integer,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE "department" (
    "id" serial PRIMARY KEY,
    "name" jsonb,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

CREATE TABLE "position" (
    "id" serial PRIMARY KEY,
    "name" jsonb,
    "created_at" timestamp default CURRENT_TIMESTAMP,
    "updated_at" timestamp default CURRENT_TIMESTAMP
);

ALTER TABLE "user" ADD FOREIGN KEY ("birth_district_id") REFERENCES "district" ("id") ON DELETE SET NULL;

ALTER TABLE "region" ADD FOREIGN KEY ("republic_id") REFERENCES "republic" ("id") ON DELETE SET NULL;

ALTER TABLE "district" ADD FOREIGN KEY ("region_id") REFERENCES "region" ("id") ON DELETE SET NULL;

ALTER TABLE "session" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id") ON DELETE CASCADE ;

ALTER TABLE "session" ADD FOREIGN KEY ("role_id") REFERENCES "role" ("id") ON DELETE CASCADE ;

-- ALTER TABLE "permission" ADD CONSTRAINT "fk_permission_parent_id" FOREIGN KEY ("parent_id") REFERENCES permission(id);



